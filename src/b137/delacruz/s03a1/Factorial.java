        package b137.delacruz.s03a1;

        // Activity:
        // Create a Java program that accepts an integer and computes
        // for the factorial value and displays it to the console.

        import java.util.Scanner;
        import java.math.BigInteger;

        public class Factorial {

            public static void main(String args[]){
                System.out.println("Factorial\n");
                int n, c;

                BigInteger i = new BigInteger("1");
                BigInteger f = new BigInteger("1");

                Scanner input = new Scanner(System.in);

                System.out.println("Input an integer: ");
                n = input.nextInt();

                for (c = 1; c <= n; c++) {
                    f = f.multiply(i);
                    i = i.add(BigInteger.ONE);
                }

                System.out.println(n + "! = " + f);
            }
        }


